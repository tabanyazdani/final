import javax.swing.JOptionPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Authenication 
{
	/**
	 * this is the method that gets the username and password 
	 * @param userNames sets the username	
	 * @param passwords sets the password
	 */
	public static void signIn(String [] userNames, String [] passwords) 
	{
		boolean wrongAccount = true;
		final int LIMIT=3;
		String uname;
		String Pass;
		int countlimit=0;
		boolean loggedIn = false;
		
		ImageIcon icone= new ImageIcon("IMG_0098.jpg", "java icon");
		
		
		while(wrongAccount)
		{
				
		String[] options = { "Customer", "Cashier"};
		String input= (String)
				JOptionPane.showInputDialog(null,"Choose an account type","Account",
						JOptionPane.QUESTION_MESSAGE, icone, options, options[1]);
		
        switch (input)
        {
	        
	        case "Cashier":
	        	JOptionPane.showMessageDialog(null, "Wrong account type");
	        	break;
	        case "Customer":
	        	countlimit=0;
	    		loggedIn = false;
	    		wrongAccount = false;
	    		while (!loggedIn)
	    		{
	    			uname=JOptionPane.showInputDialog("Enter user name");
	    			
	    			boolean correctUname = false;
	    			correctUname = uname.equalsIgnoreCase(userNames[0]);
	    			
	    			if (correctUname)
	    			{
	    			    Pass=JOptionPane.showInputDialog("Password");
	    			    
	    			    boolean correctPass = false;
		    			correctPass = Pass.equalsIgnoreCase(passwords[0]);
	    		        if(correctPass) 
	    		        {
	    		        	loggedIn = true;
	    		        	JOptionPane.showMessageDialog(null, "Welcome " + uname); 
	    		        	gambling.display();
	    	        	}    
	    		        else 
	    		        {
	    		        	//wrong password
	    					System.out.println("Fail Authenication. You have  " + (LIMIT-countlimit-1)+ " trial(s) left.");
	    					countlimit++;
	    		        }
	    			
	    	        }
	    			else
	    			{
	    				
	    				JOptionPane.showMessageDialog(null, "This Username Does Not Exist.");
	    				System.out.println("Fail Authenication. You have  " + (LIMIT-countlimit-1)+ " trial(s) left.");
	    				countlimit++;
	    			}
	    			
	    			if (countlimit==LIMIT)
	    			{
	    				loggedIn = true;
	    				wrongAccount = true;
	    				JOptionPane.showMessageDialog(null,"Contact Adminstration Call (800)544-2181");
	    			}
	    		}
	        	break;
        }
        
		
		
		}
		 
	}
}

