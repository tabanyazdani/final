
public class Person {
	
	private String Username;
	private String Password;
	/**default constructor
	 * 
	 */
	public Person() {
	}
	/**
	 * this one returns Username
	 * @return Username gets the use
	 */
	public String getUsername() {
		return Username;
	}
	/**
	 * this one sets username
	 * @param username sets the username
	 */
	public void setUsername(String username) {
		Username = username;
	}
	/**
	 * this one returns Password
	 * @return Password return password
	 */
	public String getPassword() {
		return Password;
	}
	/**
	 * this one sets password
	 * @param password gets the password
	 */
	public void setPassword(String password) {
		Password = password;
	}
	/**
	 * this is the tostring method
	 */
	@Override
	public String toString() {
		return "Person [Username=" + Username + ", Password=" + Password + "]";
	}
	/**
	 * this is the overloaded constructor 
	 * @param username gets the username
	 * @param password gets the password
	 */
	public Person(String username, String password) {
		super();
		Username = username;
		Password = password;
			}
	/**
	 * this one return username and return the information
	 * @return the count of user name and password
	 */
	public static int getCount() {
		
		return 0;
	}
	
}