import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;

import java.awt.event.ActionEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JButton;
import javax.swing.JFrame;



	public class PosDemo extends JFrame { 
		JButton jbtnRegistration = new JButton("Register an account");
		JButton jbtnSignIn = new JButton("Sign in");
		
		Registration reg;
		/**
		 * this is the constructor method
		 * this one gets the username and password to register a new account
		 */
		public PosDemo() {
			jbtnRegistration.addActionListener(new ActionListener () {
				public void actionPerformed(ActionEvent e) {
					reg = new Registration();
				}
			});
			jbtnSignIn.addActionListener(new ActionListener () {
				public void actionPerformed(ActionEvent arg0) {
					String [] US = {reg.user[0].getUsername()};
					String [] PS = {reg.user[0].getPassword()};
					
					Authenication.signIn(US, PS);
				}
			});
			
			
			setLayout( new FlowLayout());
			
			add(jbtnRegistration);
			add(jbtnSignIn);
						
			setVisible(true);
			setTitle("POS System");
			setExtendedState(MAXIMIZED_BOTH);
			add(jbtnRegistration);
	}
		/**
		 * this is the main method 
		 * @param args gets the registration details
		 */
	public static void main(String[] args) {
		
         new PosDemo();
	}
		
	}
